<%-- 
    Document   : about
    Created on : Apr 20, 2016, 12:48:01 AM
    Author     : Zack
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="Includes\header.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <style>
        p.ex1 {
            margin-right: 8cm;
            margin-left: 8cm;
            line-height: 170%;
                }
        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <link rel="stylesheet" type="text/css" href="Styles/style.css">
        <title>About</title>
    </head>
    <body>
        
        <span><img src="Images/Logo.png" width="200px" height="200px"></span>
        
        <br>
        <br>
        

        <h1  align=center id="title">About Us</h1>
        
        <br>
        
        
        <p class="ex1"><font size="3" color="white">Teamwork collaboration is an approach that is used to effectively initiate, plan, and execute large 

projects. In order to create a strong finished product with a team, having strong communication and 

organization between group members is essential. This web applications recognizes this and provides a 

platform that caters to those needs and can help users develop a sufficient finished product.

The main objective of this teamwork development application is to provide teams with an 

opportunity to organize thoughts, goals, and progress in a single place. In order to effectively work as a 

team, it is imperative that the collaborators are able to organize the different phases of project progress 

while staying synchronized with the other members of the team. This application helps users stay 

organized through the use of messaging, task distribution, and a progression calculation bar. Each user 

creates their own profile and with this profile they are able to monitor their personal progress, as well as 

the progress of their team members.</font>
         </p>
    
    </body>
    
    
</html>
<%@ include file="Includes\footer.jsp" %>