<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="Includes\header.jsp" %>
<!DOCTYPE html>
<html>
    <head>
         <link rel="stylesheet" type="text/css" href="Styles/style.css">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calendar page</title>
    </head>
    <body>
        

            <br>
            <br>
            <br>
          
            
            <table id="teamMembers">
                 <tr>
                     <th id="membersTitle">Team Members</th>
                 </tr>
                 <c:forEach var="c" items="${sessionScope.userList}">
                     <tr>
                         <td>${c.fullname} : ${c.userID} </td>
                         
                     </tr>
                 </c:forEach>
             </table>
            
            
            
            
                   
          <a id="messageBoardTitle">Events</a>
          
          
          
                            <fieldset>
        <div id ="forms"> 
        
        
        
            <form class="login" action="home" method="post" >
         <input class="register-input" name="action" type="hidden" value="events">
    
            <label class="pad_top" style="margin-right:50px;">Event Name:</label>
            <input class="register-input"  type="text" name="eventName"  required><br>
            <label class="pad_top" style="margin-right:33px;">Event Date:</label>
            <input class="register-input" type="text" name="eventDate" required><br>
            <label class="pad_top" style="margin-right:31px; height: 100px;">Event Message:</label>
            <input class="register-input" type="text" name="eventMsg" required><br>
            
            <button class="register-button" name="submit" type="submit" value="Event"> Add Event</button>
            <button class="register-button" name="reset"  type="reset" value ="Reset" >Reset</button>
                    



                

     </form>
        </div>
            
              </fieldset>
          
          
          
          
          
          <div align="center">
          <table border="1" cellpadding="5">
			<caption><h2>Events</h2></caption>
			<tr>
				<th>Event Name</th>
				<th>Event date</th>
                                <th>Message</th>
			</tr>
          
         
                    <c:forEach var="c" items="${sessionScope.usersEvents}">
                        <tr>
                            <td>${c.eventName}</td>
                            <td>${c.eventDate}</td>
                            <td>${c.eventMsg}</td>
                        </tr>
                    </c:forEach>
                             
          </table>
          
         
              <br>
              <br>
              <br>
            
        
           </body>
</html>

<%@ include file="Includes\footer.jsp" %> 