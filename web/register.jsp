
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
         <link rel="stylesheet" type="text/css" href="Styles/signupAndLogin.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register</title>
    </head>
    <body>
        
         <c:if test="${sessionScope.user != null}">
        <%@ include file="Includes\header.jsp" %>
         </c:if>

        
                            <fieldset>
        
        <c:choose>
              <c:when test="${sessionScope.user == null}">
               <div id="forms">
             </c:when>
             <c:otherwise>
                 <div id="formsUpdate">
             </c:otherwise>
        </c:choose>   

                    
              <form class="register" action="login" method="post">
                        
            <c:choose>
            <c:when test="${sessionScope.user == null}">
            <input id="page" name="action" type="hidden" value="register">
            </c:when>
            <c:otherwise>
            <input id="page" name="action" type="hidden" value="update">
            </c:otherwise>
            </c:choose>
                    
                        
             <c:choose>
            <c:when test="${sessionScope.user == null}">
                     <span class="title">Registration</span>
            </c:when>
                   <c:otherwise>
                  <span class="title">Update profile</span>
                   </c:otherwise>
            </c:choose> 
  
                   
                    
                    <legend class="register-invalid"> <output id="result"/></legend> 
                    
                      
                    <c:choose>
                        <c:when test="${sessionScope.user == null}">
                     <label class="pad_top">Full Name </label>
                        <input class="register-input" type="text" id="f" name="fullName" required> 
                    <br>
                           
                        <label class="pad_top">Email</label>
                        <input class="register-input" type="email" id="e" name="email" required><br>
             
                        <label class="pad_top">Username</label>
                        <input class="register-input" type="text" id="u" name="username" required ><br>
                        
                        <label class="pad_top">Group</label>
                        <input class="register-input" type="text" id="g" name="group" required ><br>
                        
                   <label class="pad_top">Password</label>
                   <input class="register-input" type="password" name="password" pattern=".{7,}" required>
                   
                   <br>
                       <label class="pad_top">Are you an instructor:</label>
                        <div id="radioBox">
                        <input class="radio-input" type="radio" id="r" name="instructor" value="yes" required >Yes<br>
                         <input class="radio-input" type="radio" id="r" name="instructor" value="no" required >No<br>
                   </div>
                       
                    <br>   
                        </c:when>
                        <c:otherwise>
                         <label class="pad_top">Full Name </label>
                        <input class="register-input" type="text" id="f" name="fullName" required> 
                    <br>
                           
                        <label class="pad_top">Email</label>
                        <input class="register-input" type="email" id="e" name="email" required><br>

                   <label class="pad_top">Password</label>
                   <input class="register-input" type="password" name="password" pattern=".{7,}" required>
                   
                   <br>
                        </c:otherwise>
                    </c:choose> 
            
                   
                   
                   
                   
               <c:choose>
            <c:when test="${sessionScope.user == null}">
                <button class="register-button" type="submit" value="register" > Register </button>
                <a href="login.jsp"><input type="button" value="Login" /></a>
            </c:when>
                <c:otherwise>
                    <button class="register-button" type="submit" value="update"> Update Info </button>
                    <a href=""><input type="reset" value="Reset" /></a>
                </c:otherwise>
              </c:choose>
                   
                   
               
               
         </form>
                </div>
</fieldset>
   
            

            
            
        <br>
            <br>
            <br>                  
                      
         
                           
                           

        
    
        
        
        
        
        
        
        
        
        
      
    </body>
</html>
<%@ include file="Includes\footer.jsp" %>