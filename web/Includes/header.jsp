<%-- 
    Document   : header
    Created on : Feb 3, 2016, 5:31:15 PM
    Author     : myvm
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="Styles/header.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.js"></script>
        <script type="text/javascript" src="Includes/javascript.js"></script>
        <title>Header</title>
    </head>
    <body>
       

        
        
    <div class="headerBar">
        <div>
   <div class="global-nav" data-section-term="top_nav"> 
 <div class="nav-inner">
    
    
            
<div id="container">
            
        
        <br>
        <br>
        <div role="navigation" style="display: inline-block">
            <ul class="nav js-global-action" id="global-action">
                
                
                <li class="homeTab">
                <a href="home.jsp">
                   
                    <span><img src="Images/home.png" width="20px" height="20px"></span>
                              <span class="text">Home</span>
                    
                </a>
                </li>
                
               
                
                
                            <li class="projectsTab">
                 
             <a href="projects.jsp">
                   
                    <span><img src="Images/project.png" width="20px" height="20px"></span>
                              <span class="text">Project</span>
                    
                </a>
                </li> 
                
                             <li class="calendarTab">
                 
             <a href="calendar.jsp">
                   
                    <span><img src="Images/calendar.png" width="20px" height="20px"></span>
                              <span class="text">Events</span>
                    
                </a>
                </li>
                
                <li class="feedbackTab">
                 
             <a href="feedback.jsp">
                   
                    <span><img src="Images/note.jpg" width="20px" height="20px"></span>
                              <span class="text">Feedback</span>
                    
                </a>
                </li>
                
                            <li class="aboutTab">
                 
             <a href="about.jsp">
                   
                    <span><img src="Images/question.png" width="20px" height="20px"></span>
                              <span class="text">About</span>
                    
                </a>
                </li>
                
                
                
               
                
                
                
                                             <li class="profileTab">
                 
             <a href="register.jsp">
                   
                    <span><img src="Images/profile.png" width="20px" height="20px"></span>
                              <span class="text">Profile</span>
                    
                </a>
                </li>
                <li class="logoutTab">
                <a>
                <form class="logout" action="login" method="post" >
                 <input name="action" type="hidden" value="logout"> 
                <button name="logout"  type="submit" value ="logout" >Logout</button>
                </form>
                </a>
                </li> 
                
                
                
                
                
                
                
            </ul>
            
                
          </div>
            </div>
        </div>
        
        
 
        
        
       </div> 
     
   </div> 
      
</div>               

         
 
             
             


            

         
    </body>
</html>
