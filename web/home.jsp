<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="Includes\header.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="Styles/style.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home page</title>
    </head>
    <body>
        
        
        
         <c:if test="${sessionScope.user == null}">
             <c:redirect url="login.jsp"/>
        </c:if>
        
        
        <h1 id="title">Welcome ${user.fullname}</h1>
        

                                                   
        
        <h3 id="progressOne">Project Percentage Completed: ${user.projectPercentage} %</h3>
        
         <c:if test="${user.projectPercentage != null}">
             <div class="meter">
                <span style="width: ${user.projectPercentage}%"></span>
            </div>
        </c:if>
       
        <br>
        
        <h3 id="progressTwo">Individual tasks percentage Completed: ${user.individualPercentage} %</h3>
        <c:if test="${user.individualPercentage != null}">
             <div class="meter">
                <span style="width: ${user.individualPercentage}%"></span>
            </div>
        </c:if>
        
        
        
        
               <table id="groupName">
                 <tr>
                     <th id="groupTitle">Group Number</th>
                 </tr>
                     <tr>
                         <td>${user.group} </td>
                         
                     </tr>
                
             </table>    
        
      
        
        
        
        
             <table id="teamMembers">
                 <tr>
                     <th id="membersTitle">Team Members</th>
                 </tr>
                 <c:forEach var="c" items="${sessionScope.userList}">
                     <tr>
                         <td>${c.fullname} : ${c.group} </td>
                         
                     </tr>
                 </c:forEach>
             </table>
        
        
        
        

        
         <br>
            <br>
            <br>
            <br>
            
           
                
         <div class="typeMessageBox">
                   
          <a id="messageBoardTitle">Group ${user.group} : Group Messaging</a>
          <table id="messageBoard">
        	
                    <td valign="top">
                        <div style="width:500px; height:500px; overflow:auto;">  
                    <c:forEach var="c" items="${sessionScope.usersMessage}">

                        
                           
                                         <a>${c.fullname} :  ${c.text}
                                           
                                            

                                             </a>
                                         
                                         
                                         
                                         
                                         
                                         
                
                                                      </c:forEach>
                          </div>  
                        </td>
                  
                   
                                                </table>
           
          
        <form class="messageArea" action="home" method="post">
        <input name="action" type="hidden" value="message">
        <input type="text" name="userInput" id="userInput" maxlength="200" placeholder=""><br>
   <button class="messageButton" type="submit"><img src="Images/pencil.png" alt="pencil" width="15" height="15"  />Send</button>
   </form>
             
       

          </div>
               
     
       
       
        
    </body>
</html>
<%@ include file="Includes\footer.jsp" %>