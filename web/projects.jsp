<%-- 
    Document   : projects
    Created on : Mar 8, 2016, 3:56:16 PM
    Author     : Ryan
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="Includes\header.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <link rel="stylesheet" type="text/css" href="Styles/style.css">
        <title>Projects</title>
    </head>
    <body>
        
       
        <h1 id="title">Project Progress</h1>
        
        <h3 id="progressOne">Project Percentage Completed: ${user.projectPercentage} %</h3>
        
         <c:if test="${user.projectPercentage != null}">
             <div class="meter">
                <span style="width: ${user.projectPercentage}%"></span>
            </div>
        </c:if>
       
        <br>
        
        <h3 id="progressTwo">Individual tasks percentage Completed: ${user.individualPercentage} %</h3>
        <c:if test="${user.individualPercentage != null}">
             <div class="meter">
                <span style="width: ${user.individualPercentage}%"></span>
            </div>
        </c:if>
        
            <br>
            <br>
            <br>
          
            
            <table id="teamMemberProjects">
                 <tr>
                     <th id="membersTitle">Team Members</th>
                 </tr>
                 <c:forEach var="c" items="${sessionScope.userList}">
                     <tr>
                         <td>${c.fullname} : ${c.userID} </td>
                         
                     </tr>
                 </c:forEach>
             </table>
            
          <a id="projectsBoardTitle">Group ${user.group} : Project Tasks</a>
 <div id="projectForms">      
          <table border="1" cellpadding="5">
                        
		
			<tr>
				<th>Task Name</th>
                                <th>Task ID</th>
				<th>Assigned to</th>
				<th>Due date</th>
                                <th>Priority</th>
                                <th>Status</th>
                                <th>Feedback</th>
                                <th>My Action</th>
			</tr>
          
         
                    <c:forEach var="c" items="${sessionScope.usersTasks}">
                        <tr>
                            <td>${c.task}</td>
                            <td>${c.taskID}</td>
                            <td>${c.assignTo}</td>
                            <td>${c.dueDate}</td>
                            <td>${c.priority}</td>
                            <td>${c.status} </td>
                            <td>${c.feedback}</td>
                            <td>
                            
                                        <c:if test="${user.userID == c.assignTo}">
                                            
                                             <c:if test="${!c.status.equals('In Progress')&&!c.status.equals('Completed')}">
                                                      <form  action="project" method="post">
                                                             <input name="action" type="hidden" value="updateStatus">
                                                             <input type="hidden" name="taskID" value= "${c.taskID}">
                                                             <input type="hidden" name="status" value= "In Progress">
                                                          <button type="submit">In progress</button>
                                                     
                                                       </form> 
                                                             </c:if>
                                                             
                                                          <br>
                                                          
                                                          <c:if test="${!c.status.equals('Completed')}">
                                                               <form  action="project" method="post">
                                                             <input name="action" type="hidden" value="updateStatus">
                                                             <input type="hidden" name="assignedTo" value= "${c.assignTo}">
                                                             <input type="hidden" name="taskID" value= "${c.taskID}">
                                                             <input type="hidden" name="status" value= "Completed">
                                                          <button type="submit">Completed</button>
                                                     
                                                       </form> 
                                                          </c:if>
                                                         
                                          
                  
                  
                                                       
                                                           </c:if>
                            
                            
                            </td>
                           
                            
                        </tr>
                    </c:forEach>
                             
          </table>
              
          <br>
          <br>
              
              
              <caption><h2>Add Tasks</h2></caption>
          
          
          
                           
        <div id ="forms"> 
        
        
        
            <form class="projects" action="project" method="post" >
         <input class="register-input" name="action" type="hidden" value="tasks">
    
            <label class="pad_top">Task Name </label>
            <input class="register-input"  type="text" name="task"  required><br>
            <label class="pad_top">Assign to user by userID </label>
            <input class="register-input" type="text" name="assigned" required><br>
            <label class="pad_top">Due Date (YYYY/MM/DD) </label>
            <input class="register-input" type="text" name="dueDate" required><br>
            <label class="pad_top">Priority </label>
            <input class="register-input" type="text" name="priority" required><br>
            <button class="register-button" name="submit" type="submit" value="Task"> Add Task</button>
            <button class="register-button" name="reset"  type="reset" value ="Reset" >Reset</button>
                    



                

     </form>
        </div>
            
             
              
              <br>
              <br>
              
              <div id ="forms"> 
        
        
        
            <form class="projects" action="project" method="post" >
         <input class="register-input" name="action" type="hidden" value="feedback">
    
            <label class="pad_top">Task ID </label>
            <input class="register-input"  type="text" name="taskID"  required><br>
            <label class="pad_top">Feedback Score </label>
            <input class="register-input" type="text" name="feedback" required><br>
            <button class="register-button" name="submit" type="submit" value="Task"> Add Feedback</button>                

     </form>
        </div>
          
</div>
              
              <br>
              
            
        
        
    </body>
   
</html>
<%@ include file="Includes\footer.jsp" %>

