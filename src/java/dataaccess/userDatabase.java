
package dataaccess;

import getAndSet.Events;
import getAndSet.Message;
import getAndSet.Project;
import getAndSet.Feedbacks;
import getAndSet.User;
import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.*;
import javax.swing.JOptionPane;
import java.text.SimpleDateFormat;


public class userDatabase {
    
   
////////////////////////////////////////////////////////////////////////////////
    public static int insert(User user) throws FileNotFoundException, IOException, SQLException {
        
                               // get a connection
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet checkresult=null;
        
        
                           try{
        String checkQuery = "SELECT * FROM user";
       ps = connection.prepareStatement(checkQuery);
            checkresult = ps.executeQuery();
        }catch(SQLException e){
            
        }
        
        if(checkresult!=null){
                //System.out.print("Exists");
        }else{
            createTable("user");
              System.out.print("Does not Exists");
        }
        
        
        

            //create insert query and statement
   String query="insert into user(userID,fullname,username,email,groupName,instructor,password,salt,taskCount,completedTaskCount) values(?,?,?,?,?,?,?,?,?,?)";
          PreparedStatement pst =(PreparedStatement) connection.prepareStatement(query);//puts query into statement
          
          
          try{

                      //inserts the information into the ?,?,?,? areas for values
         pst.setString(1,user.getUserID());
         pst.setString(2,user.getFullname());
         pst.setString(3,user.getUsername());
         pst.setString(4, user.getEmail()); 
         pst.setString(5,user.getGroup()); 
         pst.setString(6,user.getInstructor());
         pst.setString(7,user.getPassword()); 
         pst.setString(8,user.getSalt()); 
         pst.setDouble(9,user.getTaskCount()); 
         pst.setDouble(10,user.getCompletedTaskCount()); 

            //send the query to mySql
           
         int i = pst.executeUpdate();
          
          
          if(i==1){
              System.out.print("\nInput to SQL Successful\n");
          }else{
              System.out.print("\nInput to SQL failed\n");
          }
             

    }   catch (SQLException e) {
                System.out.print(e);
           return 0;
        }finally{
            databaseUtil.closePreparedStatement(pst);
            pool.freeConnection(connection);
        }
        return 0;

    } 
    
////////////////////////////////////////////////////////////////////////////////    
    
       public static User select(String username) throws SQLException {

        
        User user= new User();
                                // get a connection
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        
                             // create a statement
           Statement statement = connection.createStatement();

                
             try{

             ResultSet rs = statement.executeQuery("SELECT * FROM user"+" WHERE username='"+username+"'");
             
            while(rs.next()){

                user.setUserID(rs.getString("UserID"));
                user.setFullname(rs.getString("fullname"));
                user.setUsername(rs.getString("username"));
                user.setEmail(rs.getString("email"));
              user.setPassword(rs.getString("password"));
              user.setGroup(rs.getString("groupName"));
              user.setInstructor(rs.getString("instructor"));
              user.setSalt(rs.getString("salt"));
              user.setTaskCount(rs.getInt("taskCount"));
              user.setCompletedTaskCount(rs.getInt("completedTaskCount")); 
            }

            }catch(SQLException e ){

              }finally{
         
            databaseUtil.closePreparedStatement(statement);
            pool.freeConnection(connection);
            }
        return user;
   
    } 
    
////////////////////////////////////////////////////////////////////////////////
       
       public static ArrayList<User> selectAllUsers() { //used when someone is an instructor
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        String query = "SELECT * FROM user ";
        try {
            ps = connection.prepareStatement(query);
//            ps.setString(1, email);
            rs = ps.executeQuery();
            ArrayList<User> userList = new ArrayList<User>();
            
            User user = null;
            
            while (rs.next()) {
              user = new User();
              user.setUserID(rs.getString("UserID"));
              user.setFullname(rs.getString("fullname"));
              user.setUsername(rs.getString("username"));
              user.setEmail(rs.getString("email"));
              user.setPassword(rs.getString("password"));
              user.setGroup(rs.getString("groupName"));
              user.setInstructor(rs.getString("instructor"));
                userList.add(user);
            }
            return userList;
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        } finally {
            databaseUtil.closeResultSet(rs);
            databaseUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
        
    
  
}
     
       
       ////////////////////////////////////////////////////////////////////////////////
       
       public static ArrayList<User> selectGroupUsers(User member) { //used when someone is an instructor
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        String query = "SELECT * FROM user "+" WHERE groupName='"+member.getGroup()+"'";
        try {
            ps = connection.prepareStatement(query);
//            ps.setString(1, email);
            rs = ps.executeQuery();
            ArrayList<User> userList = new ArrayList<User>();
            
            User user = null;
            
            while (rs.next()) {
              user = new User();
              user.setUserID(rs.getString("UserID"));
              user.setFullname(rs.getString("fullname"));
              user.setUsername(rs.getString("username"));
              user.setEmail(rs.getString("email"));
              user.setPassword(rs.getString("password"));
              user.setGroup(rs.getString("groupName"));
              user.setInstructor(rs.getString("instructor"));
                userList.add(user);
            }
            return userList;
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        } finally {
            databaseUtil.closeResultSet(rs);
            databaseUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
        
    
  
}
       
       public static int insertMessages(Message message) throws IOException, SQLException {
 
         int i=-1;
        
                               // get a connection
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();

            // create a statement
            Statement statement = connection.createStatement();

            //create insert query and statement
         String query="insert into messages(userID,messageID,messageText,messageDate,fullname,username,groupNumber) values(?,?,?,?,?,?,?)";
          
          PreparedStatement pst =(PreparedStatement) connection.prepareStatement(query);//puts query into statement
          
          
          
          
          try{          
                      //inserts the information into the ?,?,?,? areas for values 
            pst.setString(1, message.getUserId());
            pst.setInt(2, message.getMessageID());
            pst.setString(3, message.getText());
            pst.setTimestamp(4, message.getDate());
            pst.setString(5, message.getFullname());
            pst.setString(6, message.getUsername());
            pst.setString(7, message.getGroupNumber());

            //send the query to mySql
          i = pst.executeUpdate();
             

    }   catch (SQLException e) {
                System.out.print(e);
                
           return 0;
        }finally{
            databaseUtil.closePreparedStatement(pst);
            pool.freeConnection(connection);
        }
        return i;

    } 
          
    ////////////////////////////////////////////////////////////////////////////////
     
 public static int delete(Integer messageID) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;

        String query = "DELETE FROM messages "
                                +"WHERE messageID = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setInt(1, messageID);

            return ps.executeUpdate();
            
        } catch (SQLException e) {
            System.out.println(e);
            return 0;
        } finally {
            databaseUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
 
/////////////////////////////////////////////////////////////////// 
 
 
 public static int updateUser(User user) {
        
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;

        String query = "UPDATE user SET "
                + "fullname = ?, "
                + "email = ?, "
                + "password = ?, "
                + "salt = ? "
                + "WHERE userID = ?";
                
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, user.getFullname());
            ps.setString(2, user.getEmail());
            ps.setString(3, user.getPassword());
             ps.setString(4, user.getSalt());
            ps.setString(5, user.getUserID());
           

            return ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
            return 0;
        } finally {
            databaseUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
 
 

 
    ///////////////////////////////////////////////////////////////////
    
       public static ArrayList<Message> getMessages(User user) {
           
         ArrayList<Message> usersMessage = new ArrayList<Message>();
           
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs=null;
        ResultSet checkresult=null;
        
        try{
        String checkQuery = "SELECT * FROM messages";
       ps = connection.prepareStatement(checkQuery);
            checkresult = ps.executeQuery();
        }catch(SQLException e){
            
        }
        
        if(checkresult!=null){
                System.out.print("Exists");
        }else{
            createTable("messages");
              System.out.print("Does not Exists");
        }
        
        
        String query = "SELECT * FROM messages"
                                        +" WHERE"+ user.getGroup()+" ORDER BY messageDate ASC;";
        try {
            ps = connection.prepareStatement(query);
            
            
            rs = ps.executeQuery();

             while (rs.next()) {
                Message message = new Message();
                message.setUserId(rs.getString("userID"));
                message.setMessageID(rs.getInt("messageID"));
                message.setText(rs.getString("messageText"));
                message.setDate(rs.getTimestamp("messageDate"));
                message.setFullname(rs.getString("fullname"));
                message.setUsername(rs.getString("username"));
                message.setGroupNumber(rs.getString("groupNumber"));
                usersMessage.add(message);
                
            }
          
                               
            return usersMessage;
            
        } catch (SQLException | NullPointerException e) {
            System.out.println(e);
            return null;
        } finally {
            databaseUtil.closeResultSet(rs);
            databaseUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
        

}

       
       public static ArrayList<Feedbacks> getFeedbacks(User user) {
           
         ArrayList<Feedbacks> usersFeedback = new ArrayList<Feedbacks>();
           
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs=null;
        ResultSet checkresult=null;
        
        try{
        String checkQuery = "SELECT * FROM feedbacks";
       ps = connection.prepareStatement(checkQuery);
            checkresult = ps.executeQuery();
        }catch(SQLException e){
            
        }
        
        if(checkresult!=null){
                System.out.print("Exists");
        }else{
            createTable("feedbacks");
              System.out.print("Does not Exists");
        }
        
        
        String query = "SELECT * FROM feedbacks"
                                        +" WHERE"+ user.getGroup()+" ORDER BY messageDate ASC;";
        try {
            ps = connection.prepareStatement(query);
            
            
            rs = ps.executeQuery();

             while (rs.next()) {
                Feedbacks feedbacks = new Feedbacks();
                feedbacks.setUserID(rs.getString("userID"));
                feedbacks.setFeedbackID(rs.getInt("feedbackID"));
                feedbacks.setMessageText(rs.getString("messageText"));
                usersFeedback.add(feedbacks);
                
            }
          
                               
            return usersFeedback;
            
        } catch (SQLException | NullPointerException e) {
            System.out.println(e);
            return null;
        } finally {
            databaseUtil.closeResultSet(rs);
            databaseUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
        

}
       
       
    ////////////////////////////////////////////////////////////////////////////////
     
        public static int createTable(String table) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;

        String query=null;
 try{       
        switch(table){
     case "messages": query = "create table messages "
                + "(userID varchar(100) not null, "
                + "messageID int(100) not null, "
                + "messageText varchar(100) not null, "
                + "messageDate timestamp not null, "
                + "fullname varchar(20) not null, "
                + "username varchar(20)not null, "
                + "groupNumber varchar(20) not null , "
                + "PRIMARY KEY (messageID)) ";
                break;
     case "user": query = "create table user "
             + "(userID varchar(100) not null, "
             + "fullname varchar(45) not null, "
             + "username varchar(45) not null, "
             + "email varchar(100) not null, "
             + "groupName varchar(45) not null, "
             + "instructor varchar(3) not null not null, "
             + "password varchar(100) not null, "
             + "salt varchar(100) not null, "
             + "taskCount double(3,2), "
              + "completedTaskCount double(3,2), "
             + "PRIMARY KEY (userID)) ";
             break;
     case "project": query = "create table project( "
             + "taskID varchar(100) not null, "
             + "taskName varchar(100) not null, "
             + "assignedTo varchar(45) not null, "
             //+ "dueDate timestamp not null,"
             + "dueDate date not null, "
             + "status varchar(15), "
             + "feedback varchar(15), "
             + "priority varchar(15) not null, "
             + "primary key (taskID));";
             break;
     case "events": query = "create table events( "
             + "eventID varchar(45) not null, "
             + "eventName varchar(45) not null, "
             + "eventDate date not null, "
             + "eventMsg varchar(45) not null , "
             + "primary key (eventID));";
             break;
     case "feedback": query = "create table feedbacks( "
             + "userID varchar(100) not null, "
             + "feedbackID varchar(45) not null, "
             + "messageText varchar(100) not null, "
             + "PRIMARY KEY (userID)) ";
             break;
        }
        
            ps = connection.prepareStatement(query);
             
             
             int returned=ps.executeUpdate();
                //JOptionPane.showMessageDialog(null,returned);
            return returned;
            
        } catch (SQLException e) {
            System.out.println(e);
            return 0;
        } finally {
            databaseUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
      
 /////////////////////////////////////////////////////////////////////// 
       
      public static ArrayList<Project> getTasks(Project project2) {
           
         ArrayList<Project> usersProject = new ArrayList<Project>();
           
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs=null;
        ResultSet checkresult=null;
        
        try{
        String checkQuery = "SELECT * FROM project ORDER BY dueDate ASC";
       ps = connection.prepareStatement(checkQuery);
            checkresult = ps.executeQuery();
        }catch(SQLException e){
            
        }
        
        if(checkresult!=null){
                System.out.print("Exists");
        }else{
            createTable("project");
              System.out.print("Does not Exists");
        }
        
        String query = "SELECT * FROM project ORDER BY dueDate ASC";
        
                                        
        try {
            
            ps = connection.prepareStatement(query);
            
            rs = ps.executeQuery();

             while (rs.next()) {
                Project project = new Project();
                project.setTaskID(rs.getString("taskID"));
                project.setTask(rs.getString("taskName"));
                project.setAssignTo(rs.getString("assignedTo"));
                //project.setDueDate(rs.getTimestamp("dueDate"));
                project.setDueDate(rs.getDate("dueDate"));
                project.setFeedback(rs.getString("feedback"));
                project.setPriority(rs.getString("priority"));
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                Date date = new Date();
                Date date2 = rs.getDate("dueDate");
                project.setStatus(rs.getString("status"));
                //Date date = sdf.format(date);
                //Date date2 = sdf.parse(rs.getDate("dueDate"));
                
                //if(date2.before(date) || date2.equals(date))
              //  { project.setStatus("Complete"); }
               // else
               // { project.setStatus("In progress"); }
                
                //project.setFeedback(rs.getString("feedback"));
                //project.setFeedback("great");
                
                usersProject.add(project);
                
                
                //JOptionPane.showMessageDialog(null, project.getTask());
            }
          
                               
            return usersProject; 
            
             } catch (SQLException | NullPointerException e) {
            System.out.println(e);
            return null;
        } finally {
            databaseUtil.closeResultSet(rs);
            databaseUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
        
      }
//////////////////////////////////////////////////////////////////////////////


 public static int addFeedback(Project project) {
     
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;

        String query = "update project set feedback = ? where taskID = ?";

        try {
            ps = connection.prepareStatement(query);
            
            ps.setString(1, project.getFeedback());
            ps.setString(2, project.getTaskID());

            return ps.executeUpdate();
            
        } catch (SQLException e) {
            System.out.println(e);
            return 0;
        } finally {
            databaseUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
/////////////////////////////////////////////////////////////////////////////// 

 
 
 public static int updateTaskCount(Project project,User user) {
     

        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;


        String query = "update user set taskCount = ? where userID = ?";

        try {
            ps = connection.prepareStatement(query);
            
            ps.setDouble(1,user.getTaskCount());
            ps.setString(2, project.getAssignTo());

            return ps.executeUpdate();
            
        } catch (SQLException e) {
            System.out.println(e);
            return 0;
        } finally {
            databaseUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
 

////////////////////////////////////////////////////////////////////////////////      
      
 
        public static int getTaskCount(String userID) throws SQLException {

        User user= new User();
                                // get a connection
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
         ResultSet rs=null;
                             // create a statement
           Statement statement = connection.createStatement();

           
           
           
           String query = "SELECT taskCount FROM user"+" WHERE userID='"+userID+"'";
           
        try {
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {

                user.setTaskCount(rs.getInt("taskCount"));
                
                
            }

            }catch(SQLException e ){

              }finally{
         
            databaseUtil.closePreparedStatement(statement);
            pool.freeConnection(connection);
            }
        return 0;
   
    } 
    
////////////////////////////////////////////////////////////////////////////////
 

        public static int getAssignTaskCount(String assignedTo) throws SQLException {

        User user= new User();
                                // get a connection
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
         ResultSet rs=null;
                             // create a statement
           Statement statement = connection.createStatement();

           
           
           
           String query = "SELECT taskCount FROM user"+" WHERE userID='"+assignedTo+"'";
           
        try {
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {

                user.setAssignedTaskCount(rs.getInt("taskCount"));
                
                
            }

            }catch(SQLException e ){

              }finally{
         
            databaseUtil.closePreparedStatement(statement);
            pool.freeConnection(connection);
            }
        return 0;
   
    } 
    

        
        
   ////////////////////////////////////////////////////////////////////////////////         
        
      
      public static int updateAssignTaskCount(Project project,User user) {
     

        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;


        String query = "update user set taskCount = ? where userID = ?";

        try {
            ps = connection.prepareStatement(query);
            
            ps.setDouble(1,user.getAssignedTaskCount());
            ps.setString(2, project.getAssignTo());

            return ps.executeUpdate();
            
        } catch (SQLException e) {
            System.out.println(e);
            return 0;
        } finally {
            databaseUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
 

////////////////////////////////////////////////////////////////////////////////     
        
  public static int getAllTaskCount(User user) throws SQLException {

      
                                // get a connection
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
         ResultSet rs=null;
                             // create a statement
           Statement statement = connection.createStatement();


           String query = "SELECT SUM(taskCount) AS total FROM user where groupName="+user.getGroup();
           
        try {
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {

               user.setTotalProjectTasks(rs.getInt("total"));
               
   
            }

            }catch(SQLException e ){

              }finally{
         
            databaseUtil.closePreparedStatement(statement);
            pool.freeConnection(connection);
            }
        
       
        return 0;
   
    } 

 //////////////////////////////////////////////////////////////////////////////
  
  
  
      public static int insertTasks(Project project) throws IOException, SQLException {
 
         int i=-1;
       // JOptionPane.showMessageDialog(null, "in insert task");
                               // get a connection
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();

            // create a statement
            Statement statement = connection.createStatement();

            //create insert query and statement
         String query="insert into project(taskID,taskName,assignedTo,dueDate,priority,status) values(?,?,?,?,?,?)";
          
          PreparedStatement pst =(PreparedStatement) connection.prepareStatement(query);//puts query into statement
          
          
          
          
          try{          
                      //inserts the information into the ?,?,?,? areas for values 
            pst.setString(1, project.getTaskID());
            pst.setString(2, project.getTask());
            pst.setString(3, project.getAssignTo());
            pst.setDate(4, project.getDueDate());
            pst.setString(5, project.getPriority());
            pst.setString(6, "Backlog");
            
            //send the query to mySql
          i = pst.executeUpdate();
             

    }   catch (SQLException e) {
                System.out.print(e);
                
           return 0;
        }finally{
            databaseUtil.closePreparedStatement(pst);
            pool.freeConnection(connection);
        }
          
        return i;

    }
      
      
 ///////////////////////////////////////////////////////////////////////////////     
      
   public static int updateStatus(Project project) {
     
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;

        String query = "update project set status = ? where taskID = ?";

        try {
            ps = connection.prepareStatement(query);
            
            ps.setString(1, project.getStatus());
            ps.setString(2, project.getTaskID());

            return ps.executeUpdate();
            
        } catch (SQLException e) {
            System.out.println(e);
            return 0;
        } finally {
            databaseUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
   
 
   
////////////////////////////////////////////////////////////////////////////////
     

 public static int updateCompletedTaskCount(User user,Project project) {
     

        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;


        String query = "update user set completedTaskCount = ? where userID = ?";

        try {
            ps = connection.prepareStatement(query);
            
            ps.setDouble(1,user.getCompletedTaskCount());
            ps.setString(2, project.getAssignTo());

            return ps.executeUpdate();
            
        } catch (SQLException e) {
            System.out.println(e);
            return 0;
        } finally {
            databaseUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
 
 
 ////////////////////////////////////////////////////////////////////////////////
 
  public static int getAllCompletedTaskCount(User user) throws SQLException {

      
                                // get a connection
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
         ResultSet rs=null;
                             // create a statement
           Statement statement = connection.createStatement();


           String query = "SELECT SUM(completedTaskCount) AS total FROM user";
           
        try {
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {

               user.setTotalCompletedTasks(rs.getInt("total"));
               
   
            }

            }catch(SQLException e ){

              }finally{
         
            databaseUtil.closePreparedStatement(statement);
            pool.freeConnection(connection);
            }
        
       
        return 0;
   
    } 

 //////////////////////////////////////////////////////////////////////////////
    
      
 
        public static int getCompletedTaskCount(String userID) throws SQLException {

        User user= new User();
                                // get a connection
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
         ResultSet rs=null;
                             // create a statement
           Statement statement = connection.createStatement();

           
           
           
           String query = "SELECT completedTaskCount FROM user"+" WHERE userID='"+userID+"'";
           
        try {
            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {

                user.setCompletedTaskCount(rs.getInt("completedTaskCount"));

            }

            }catch(SQLException e ){

              }finally{
         
            databaseUtil.closePreparedStatement(statement);
            pool.freeConnection(connection);
            }
        return 0;
   
    } 
    


///////////////////////////////EVENTS///////////////////////////////////////////      
      
 public static ArrayList<Events> getEvents(Events event) {
           
         ArrayList<Events> usersEvents = new ArrayList<Events>();
           
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs=null;
        ResultSet checkresult=null;
        
        try{
        String checkQuery = "SELECT * FROM events ORDER BY eventDate ASC";
       ps = connection.prepareStatement(checkQuery);
            checkresult = ps.executeQuery();
        }catch(SQLException e){
            
        }
        
        if(checkresult!=null){
                System.out.print("Exists");
        }else{
            createTable("events");
              System.out.print("Does not Exists");
        }
        
        
        String query = "SELECT * FROM events ORDER BY eventDate ASC";
        try {
            ps = connection.prepareStatement(query);
            
            
            rs = ps.executeQuery();

             while (rs.next()) {
                Events events = new Events();
               //events.setUserId(rs.getString("userID"));
               //events.setMessageID(rs.getInt("messageID"));
                //events.setText(rs.getString("messageText"));
                events.setEventID(rs.getString("eventID"));
                events.setEventName(rs.getString("eventName"));
                events.setEventDate(rs.getDate("eventDate"));
                events.setEventMsg(rs.getString("eventMsg"));
               //events.setDate(rs.getTimestamp("messageDate"));
                //events.setFullname(rs.getString("fullname"));
                //events.setUsername(rs.getString("username"));
                //events.setGroupNumber(rs.getString("groupNumber"));
                usersEvents.add(events);
                
            }
          
                               
            return usersEvents;
            
        } catch (SQLException | NullPointerException e) {
            System.out.println(e);
            return null;
        } finally {
            databaseUtil.closeResultSet(rs);
            databaseUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
        

}     
      
      
   public static int insertEvents(Events event) throws IOException, SQLException {
 
         int i=-1;
       
                               // get a connection
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();

            // create a statement
            Statement statement = connection.createStatement();

            //create insert query and statement
         String query="insert into events(eventID,eventName,eventDate,eventMsg) values(?,?,?,?)";
          
          PreparedStatement pst =(PreparedStatement) connection.prepareStatement(query);//puts query into statement
          
          
          
          
          try{          
                      //inserts the information into the ?,?,?,? areas for values 
            pst.setString(1, event.getEventID());
            pst.setString(2, event.getEventName());
            pst.setDate(3, event.getEventDate());
            pst.setString(4, event.getEventMsg());

            //send the query to mySql
          i = pst.executeUpdate();
             

    }   catch (SQLException e) {
                System.out.print(e);
                
           return 0;
        }finally{
            databaseUtil.closePreparedStatement(pst);
            pool.freeConnection(connection);
        }
        return i;

    }
           
      
  
      
      
      
      
      
      
      
       
       
}
