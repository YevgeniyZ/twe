
package getAndSet;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Random;


public class User implements Serializable {
   
    DecimalFormat twoDecimalPlaces = new DecimalFormat("#.##");
    
    private String password;
    private String email;
    private String username;
    private String fullname;
    private String group;
    private String salt;
    private double individualPercentage;
    private double projectPercentage;
    private double taskCount;
    private double completedTasks;
    private double totalCompletedTasks;
     private double totalTaskCount;
     private double assignedTaskCount;
    
    String message; //made up variable for message
    String feedback;
    
    private String userID;
    
    private String instructor;
    
    public User(){
        
        password=null;
        email=null;
        username=null;
        fullname=null;
        group=null;
        userID=null;
        salt=null;
        instructor="n";
        individualPercentage=0;
        projectPercentage=0;
        taskCount=0;
        completedTasks=0;
        totalCompletedTasks=0;
         totalTaskCount=0;
         assignedTaskCount=0;
        
        
        
    }
    
////////////////////////////////////////////////////    
    public void setPassword(String password){
        this.password=password;
       
    }
    
    public String getPassword(){
        return password;
    }
////////////////////////////////////////////////////    
    public void setEmail(String email){
        this.email=email;
       
    }
    
    public String getEmail(){
        return email;
    }
////////////////////////////////////////////////////   
    public void setUsername(String username){
        this.username=username;
       
    }
    
    public String getUsername(){
        return username;
    }
    
////////////////////////////////////////////////////   
    public void setFullname(String fullname){
        this.fullname=fullname;
       
    }
    
    public String getFullname(){
        return fullname;
    }
    
 ////////////////////////////////////////////////////   
    
    public void setGroup(String group){
        this.group=group;
       
    }
    
    public String getGroup(){
        return group;
    }
    
 ////////////////////////////////////////////////////   
    
   public String createUserId(){
        int randomInt=0;
        String userIDstring;
        
        Random randomGenerator = new Random();
    for (int idx = 1; idx <= 10; ++idx){
      randomInt = randomGenerator.nextInt(100000);
        
    }
    
    userIDstring=Integer.toString(randomInt);
       
        return userIDstring;
    }
    
    
    
    public void setUserID(String userID){
        this.userID=userID;
       
    }
    
    public String getUserID(){
        return userID;
    }
    
////////////////////////////////////////////////////    
    
    public void setInstructor(String instructor){
        this.instructor=instructor;
       
    }
    
    public String getInstructor(){
        return instructor;
    }
////////////////////////////////////////////////////////////////////////    
    
    public void setTotalProjectTasks(int totalTasks){
        this.totalTaskCount=totalTasks;
    }
    
        public void setTotalCompletedTasks(int totalCompletedTasks){
        this.totalCompletedTasks=totalCompletedTasks;
    }
    
    

    public double getIndividualPercentage(){
        
        if(this.taskCount!=0){
        return Double.valueOf(twoDecimalPlaces.format((this.completedTasks/this.taskCount)*100));
        }else{
            return 0;
        }
    }
    
    public double getProjectPercentage(){
       
        if(this.totalTaskCount!=0){
        return Double.valueOf(twoDecimalPlaces.format((this.totalCompletedTasks/this.totalTaskCount)*100));
        }else{
            return 0;
        }
                
                
                
                
    }
 ///////////////////////////////////////////////////////////////////////   
    
    //made up methods for message page display to work.
    public void setMessage(String m){
        message = m;
    }
    public String getMessage(){
        return message;
    }
    
    public void setFeedback(String m){
        feedback = m;
    }
    public String getFeedback(){
        return feedback;
    }
    
    
    public void setSalt(String salt) {
        this.salt = salt;
    }

     public String getSalt(){
        return salt;
    }
    
    
    ////////////////////////////////////////////////////    
    public void setTaskCount(int taskCount){
        this.taskCount=taskCount;
       
    }
    
    
        public void addTaskCount(int count){
        this.taskCount+=count;
       
    }
    
    
    
    public double getTaskCount(){
        
        return Double.valueOf(twoDecimalPlaces.format(this.taskCount));
    }
    
    
    
        public void setAssignedTaskCount(int assignedTaskCount){
        this.assignedTaskCount=assignedTaskCount;
       
    }
    
    
        public void addAssignedTaskCount(int count){
        this.assignedTaskCount+=count;
       
    }
    
    
    
    public double getAssignedTaskCount(){
        
        return Double.valueOf(twoDecimalPlaces.format(this.assignedTaskCount));
    }
    
    
    
    
//////////////////////////////////////////////////////////    
 public void setCompletedTaskCount(int completedTasks){
        this.completedTasks=completedTasks;
 }   
 
  public double getCompletedTaskCount(){

        return this.completedTasks;
 }  
  
 public void addCompletedTasksCount(int count){
        this.completedTasks+=count;
       
    }
    

////////////////////////////////////////////////////  
    
    
    
    
    
}
