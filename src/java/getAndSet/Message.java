/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package getAndSet;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Random;

/**
 *
 * @author Ryan
 */
public class Message implements Serializable {
    
    private String text;
    private String fname;
    private String uname;
    private String userId;
    private Timestamp date;
    private Integer messageID;
    private String groupNumber;


    public Message(){
        this.text=text;
        this.fname=fname;
        this.uname=uname;
        this.date=date;
        this.userId=userId;
        this.messageID=messageID;
        this.groupNumber=groupNumber;

    }
    
    public Message(String text){
        this.text=text;
    }
    public void setText(String text){
        this.text=text;
    }

    public String getText(){
        return text;
    }
    
     public String getFullname() {
        return fname;
    }

    public void setFullname(String fname) {
        this.fname = fname;
    }
    
            public String getUsername() {
        return uname;
    }

    public void setUsername(String uname) {
        this.uname = uname;
    }
    
     public String getUserId() {
    
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
   public void setMessageID(int messageID){
        this.messageID=messageID;
    }

    public int getMessageID(){
        return messageID;
    }
    
    
    
  public Timestamp getCurrentDate(){
        

  Timestamp stamp = new Timestamp(System.currentTimeMillis());
    //Date date = new Date(stamp.getTime());
            
            
                return stamp;
    }
    
    
      public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }
    
    public int createMessageId(){
        int randomInt=0;
        
        Random randomGenerator = new Random();
    for (int idx = 1; idx <= 10; ++idx){
      randomInt = randomGenerator.nextInt(10000);
        
    }
       
        return randomInt;
    }
    
     
    public void setGroupNumber(String groupNumber){
        this.groupNumber=groupNumber;
    }

    public String getGroupNumber(){
        return groupNumber;
    }
    
    
  
}